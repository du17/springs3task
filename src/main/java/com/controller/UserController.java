package com.controller;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dao.UserDao;
import com.service.UserService;

import com.model.User;

@Controller
public class UserController {
	 
	 @RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	    public String welcomePage(Model model) {
	        model.addAttribute("title", "Welcome");
	        model.addAttribute("message", "This is welcome page!");
	        return "welcome";
	    }
	 
	   /* @RequestMapping(value = "/admin", method = RequestMethod.GET)
	    public String adminPage(Model model) {
	        return "adminPage";
	    }*/
	 
	    @RequestMapping(value = "/login", method = RequestMethod.GET)
	    public String loginPage(Model model ) {
	         
	        return "login";
	    }
	 
	    @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
	    public String logoutSuccessfulPage(Model model) {
	        model.addAttribute("title", "Logout");
	        return "logoutSuccesful";
	    }
	 
	    @RequestMapping(value = "/userFiles", method = RequestMethod.GET)
	    public String userFiles(Model model, Principal principal) {
	 
	        // After user login successfully.
	        String userName = principal.getName();
	 
	        System.out.println("User Name: "+ userName);
	 
	        return "userFiles";
	    }
	 
	   	

	    @Autowired
	    public UserService userService;

	    @RequestMapping(value = "/register", method = RequestMethod.GET)
	    public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {
	      ModelAndView mav = new ModelAndView("register");
	      mav.addObject("user", new User());

	      return mav;
	    }

	    @RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
	    public ModelAndView addUser(HttpServletRequest request, HttpServletResponse response,
	        @ModelAttribute("user") User user) {

	      
	      System.out.println("User Name: "+ user.getUsername());
	      System.out.println("fullname : "+ user.getFullname());
	      userService.register(user);
	      return new ModelAndView("userFiles", "fullname", user.getFullname());
	    }
	    
}