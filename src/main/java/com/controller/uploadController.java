package com.controller;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.aws.GenerationParams;
import com.aws.PreSignedUrlGenerator;
import com.model.S3Options;

@Controller
@PropertySource("classpath:amazonaws.properties")
public class uploadController 
{
	@Value("${accessKey}")
	private String accessKey;

	@Value("${clientSecret}")
	private String secretKey;

	@Value("${bucket}")
	private String bucketName;

	
	@RequestMapping(method=RequestMethod.POST,value = "/Upload")
	public String handleFileUpload(@RequestParam("file") MultipartFile file
										){
		URL url = null;
		AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider());
		
	/*long userId = (long) request.getSession().getAttribute("userId");*/
     
	S3Options s3Options = new S3Options(bucketName,accessKey,secretKey);	
	GenerationParams generationParams = new GenerationParams(file, false);
     try {
		System.out.println("Generating pre-signed URL.");
		
		PreSignedUrlGenerator generatePresignedUrl = 
			    new PreSignedUrlGenerator(s3Options);
		

		 url = generatePresignedUrl.generate(generationParams); 

		try {
			UploadObject(url);
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Pre-Signed URL = " + url.toString());
	} catch (AmazonServiceException exception) {
		System.out.println("Caught an AmazonServiceException, " +
				"which means your request made it " +
				"to Amazon S3, but was rejected with an error response " +
		"for some reason.");
		System.out.println("Error Message: " + exception.getMessage());
		System.out.println("HTTP  Code: "    + exception.getStatusCode());
		System.out.println("AWS Error Code:" + exception.getErrorCode());
		System.out.println("Error Type:    " + exception.getErrorType());
		System.out.println("Request ID:    " + exception.getRequestId());
	} catch (AmazonClientException ace) {
		System.out.println("Caught an AmazonClientException, " +
				"which means the client encountered " +
				"an internal error while trying to communicate" +
				" with S3, " +
		"such as not being able to access the network.");
		System.out.println("Error Message: " + ace.getMessage());
	}
	return url.toString();
    
}

	public static void UploadObject(URL url) throws IOException
	{
		HttpURLConnection connection=(HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("PUT");
		OutputStreamWriter out = new OutputStreamWriter(
				connection.getOutputStream());
		out.write();
		out.close();
		int responseCode = connection.getResponseCode();
		System.out.println("Service returned response code " + responseCode);
	
	}
}



