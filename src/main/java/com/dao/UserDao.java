package com.dao;
import java.util.List;

import com.model.User;

public interface UserDao {

  void register(User user);
   List list();
   User get(Long id);
}
