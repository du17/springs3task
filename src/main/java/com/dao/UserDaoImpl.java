package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.model.User;
import com.service.UserService;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

		
  @Autowired
  DataSource dataSource;

  @Autowired
  JdbcTemplate jdbcTemplate;

  public void register(User user) {

    String sql = "insert into users values(?,?,?)";
    System.out.println(user);
    jdbcTemplate.update(sql, new Object[] { user.getUsername(), user.getPassword(), user.getFullname() });
  }

public List list() {
	// TODO Auto-generated method stub
	return null;
}

public User get(Long id) {
	// TODO Auto-generated method stub
	return null;
}

  

}

class UserMapper implements RowMapper<User> {

  public User mapRow(ResultSet rs, int arg1) throws SQLException {
    User user = new User();

    user.setUsername(rs.getString("username"));
    user.setPassword(rs.getString("password"));
    user.setFullname(rs.getString("fullname"));
  
    return user;
  }
}